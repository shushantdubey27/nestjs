import { Body, Controller, Get, Param, Post, Query, Req, Res } from '@nestjs/common';
import { Request, Response, response } from 'express';
import { userCreateDto } from 'src/users/dtos/user.dtos';

@Controller('users')
export class UsersController {
  @Get()
  getUser() {
    return { username: 'Shushant', email: 'shsushant@gmail.com' };
  }
  @Post()
  createUser(@Req() request: Request, @Res() response: Response) {
    console.log(request.body);
    response.send(request.body);
  }
  @Post('jj')
  workWithParams(@Body() userPayload: userCreateDto, @Param('id') id: number) {
    console.log(userPayload.username, id);
    return 'Hello';
  }
  @Post('hello')
  workWithQuery(@Query('sortBy') sortBy: string) {
    console.log(sortBy);
    return 'Hello';
  }

  @Post('/testing/:id')
  testing(@Param('id') id: string, @Query('sortBy') sortBy: string) {
    console.log(id, sortBy);
    return {};
  }
}
